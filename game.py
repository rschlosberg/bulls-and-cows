from random import randint

# Generates a list of four, unique, single-digit numbers
def generate_secret():
    secret_num_list = []
    while len(secret_num_list)< 4:
        new_number = randint(1,9)
        if new_number not in secret_num_list:
            secret_num_list.append(new_number)
    return secret_num_list


def play_game():
    print("Let's play Bulls and Cows!")
    print("--------------------------")
    print("I have a secret four numbers.")
    print("Can you guess it?")
    print("You have 20 tries.")
    print()

    secret = generate_secret()
    number_of_guesses = 20

    for i in range(number_of_guesses):
        prompt = "Type in guess #" + str(i + 1) + ": "
        guess = input(prompt)

        # TODO:
        while len(guess) != 4:
            print("You must enter a four-digit number")
            guess = input(prompt)

        # TODO:
        def string_to_num_list(guess):
            guess_list = []
            for n in guess:
                guess_list.append(int(n))
            return guess_list

        # write a function and call it here to:
        # convert the string in the variable guess to a list
        # of four single-digit numbers
        guess_list = string_to_num_list(guess)

        # TODO:
        def exact_match_count(secret, guess_list):
            pairs = zip(secret, guess_list)
            matches = 0
            for a,b in pairs:
                if a == b:
                    matches = matches + 1
            return matches 

        # write a function and call it here to:
        # count the number of exact matches between the
        # converted guess and the secret

        # TODO:
        def cow_match_count(secret, guess_list):
            common_entries = 0
            for j in secret: 
                if j in guess_list:
                    common_entries = common_entries + 1
            return common_entries
            
        # write a function and call it here to:
        # count the number of common entries between the
        # converted guess and the secret

        # TODO:
        # if the number of exact matches is four, then
        # the player has correctly guessed! tell them
        # so and return from the function.
        bulls = exact_match_count(secret, guess_list)
        cows = cow_match_count(secret, guess_list)

        if bulls == 4:
            print("You won! You've guessed the number exactly!")
            exit()

        # TODO:
        print("Bulls:", bulls)
        # report the number of "bulls" (exact matches)
        # TODO:
        print("Cows:", cows)
        # report the number of "cows" (common entries - exact matches)

    # TODO:
    # If they don't guess it in 20 tries, tell
    # them what the secret was.
    if bulls != 4:
        print("I'm sorry, you did not guess the secret number:", secret)


# Runs the game
def run():
    # TODO: Delete the word pass, below, and have this function:
    # Call the play_game function
    # Ask if they want to play again
    # While the person wants to play the game
    #   Call the play_game function
    #   Ask if they want to play again
    play_game()
    
    extra_games = 0
    while extra_games < 2:
        answer = input("Would you like to play again? yes or no?")
        if answer == "yes":
            play_game()
            extra_games = extra_games + 1
        elif answer == "no":
            exit()
        else:
            print("Please answer yes or no")
    return



if __name__ == "__main__":
    run()
